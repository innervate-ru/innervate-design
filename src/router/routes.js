
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/input.vue') },
      { path: 'select', component: () => import('pages/select.vue') },
      { path: 'button', component: () => import('pages/button.vue') },
      { path: 'link', component: () => import('pages/link.vue') },
      { path: 'checkbox', component: () => import('pages/checkbox.vue') },
      { path: 'radio', component: () => import('pages/radio.vue') },
      { path: 'toggle', component: () => import('pages/toggle.vue') },
      { path: 'chip', component: () => import('pages/chip.vue') },
      { path: 'tab', component: () => import('pages/tab.vue') },
      { path: 'notify', component: () => import('pages/notify.vue') },
      { path: 'tooltip', component: () => import('pages/tooltip.vue') },
      { path: 'dialog', component: () => import('pages/dialog.vue') },
      { path: 'banner', component: () => import('pages/banner.vue') },
      { path: 'timeline', component: () => import('pages/timeline.vue') },
      { path: 'table', component: () => import('pages/table.vue') },
      { path: 'text', component: () => import('pages/text.vue') },
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
